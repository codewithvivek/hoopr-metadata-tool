// API Keys and Secrets
var YOUR_API_KEY = "AIzaSyBead7MTGPbzW3NT66yYqkBG-gQjdkYpzM";

// Input Fields And Output Fields
let stats = document.getElementById("stats");
let ytinput = document.getElementById("yt-input");
let fbinput = document.getElementById("fb-input");
let name = document.getElementById("name");
let desc = document.getElementById("desc");
let title = document.getElementById("title");
let views = document.getElementById("views");
let likes = document.getElementById("likes");
let comments = document.getElementById("comments");
let uploaddate = document.getElementById("uploadedon");

// Buttons
let ytbtn = document.getElementById("btn-yt");
let fbbtn = document.getElementById("btn-fb");

// Event Listeners
ytbtn.addEventListener("click", hitYTApi);
fbbtn.addEventListener("click", hitFBApi);

// Function Definitions

// FB Graph API:
function hitFBApi(e) {
  e.preventDefault();
  let value = fbinput.value;

  FB.api(
    `/${value}`,
    "GET",
    {
      fields:
        "created_time,from,description,likes.limit(0).summary(1),comments.limit(0).summary(1),views",
      access_token:
        "EAAFzSw1o3ZAIBAG9YzMZCZCZBAvfXzBcMYp6krPugFBVEz3hi4V6HP3toUv7p1QyCEz0ZAsi7e6ksjKvxDA11wbaWm0UW6E9cDG6S5KA6vnIQx6EW3OzQkNxEqRWaG8ep2ZBwb4I70AhNpImqoFOxY5OZAHVtUIGOc2ZAyrLtCcu9yz12o2Ccqh3zInfa0EDmC73bR4SOUG5FRJNXedrYsoK",
    },
    function (response) {
      console.log(response);
      stats.innerHTML = "Stats:";
      name.innerHTML = `Name: ${response.from.name}`;
      desc.innerHTML = `Description: ${response.description}`;
      views.innerHTML = `Views: ${response.views.toLocaleString("en-IN")}`;
      likes.innerHTML = `Likes: ${response.likes.summary.total_count.toLocaleString(
        "en-IN"
      )}`;
      comments.innerHTML = `Comments: ${response.comments.summary.total_count.toLocaleString(
        "en-IN"
      )}`;
      uploaddate.innerHTML = `Uploaded On: ${new Date(
        response.created_time
      ).toDateString()}`;
    }
  );
}

// YouTube API v3:
function hitYTApi(e) {
  e.preventDefault();
  let value = ytinput.value;
  let url = value.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
  id = url[2] !== undefined ? url[2].split(/[^0-9a-z_\-]/i)[0] : url[0];

  var URL = `https://www.googleapis.com/youtube/v3/videos?id=${id}&key=${YOUR_API_KEY}&part=snippet,contentDetails,statistics,status`;
  axios({
    method: "get",
    url: URL,
  })
    .then((response) => {
      console.log(response.data);
      stats.innerHTML = "Stats:";
      title.innerHTML = `Title: ${response.data.items[0].snippet.title}`;
      desc.innerHTML = `Description: ${response.data.items[0].snippet.description}`;
      views.innerHTML = `Views: ${parseInt(
        response.data.items[0].statistics.viewCount
      ).toLocaleString("en-IN")}`;
      likes.innerHTML = `Likes: ${parseInt(
        response.data.items[0].statistics.likeCount
      ).toLocaleString("en-IN")}`;
      comments.innerHTML = `Comments: ${parseInt(
        response.data.items[0].statistics.commentCount
      ).toLocaleString("en-IN")}`;
      uploaddate.innerHTML = `Uploaded On: ${new Date(
        response.data.items[0].snippet.publishedAt
      ).toDateString()}`;
    })
    .catch((err) => console.log(err));
}

// Reference: https://developers.google.com/youtube/v3/getting-started

// Description: This example retrieves a video resource and identifies several
//              resource parts that should be included in the API response.

// Sample API response:

// {
//  "kind": "youtube#videoListResponse",
//  "etag": "\"UCBpFjp2h75_b92t44sqraUcyu0/sDAlsG9NGKfr6v5AlPZKSEZdtqA\"",
//  "videos": [
//   {
//    "id": "7lCDEYXw3mM",
//    "kind": "youtube#video",
//    "etag": "\"UCBpFjp2h75_b92t44sqraUcyu0/iYynQR8AtacsFUwWmrVaw4Smb_Q\"",
//    "snippet": {
//     "publishedAt": "2012-06-20T22:45:24.000Z",
//     "channelId": "UC_x5XG1OV2P6uZZ5FSM9Ttw",
//     "title": "Google I/O 101: Q&A On Using Google APIs",
//     "description": "Antonio Fuentes speaks to us and takes questions on working with Google APIs and OAuth 2.0.",
//     "thumbnails": {
//      "default": {
//       "url": "https://i.ytimg.com/vi/7lCDEYXw3mM/default.jpg"
//      },
//      "medium": {
//       "url": "https://i.ytimg.com/vi/7lCDEYXw3mM/mqdefault.jpg"
//      },
//      "high": {
//       "url": "https://i.ytimg.com/vi/7lCDEYXw3mM/hqdefault.jpg"
//      }
//     },
//     "categoryId": "28"
//    },
//    "contentDetails": {
//     "duration": "PT15M51S",
//     "aspectRatio": "RATIO_16_9"
//    },
//    "statistics": {
//     "viewCount": "3057",
//     "likeCount": "25",
//     "dislikeCount": "0",
//     "favoriteCount": "17",
//     "commentCount": "12"
//    },
//    "status": {
//     "uploadStatus": "STATUS_PROCESSED",
//     "privacyStatus": "PRIVACY_PUBLIC"
//    }
//   }
//  ]
// }
